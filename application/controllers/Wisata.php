<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class wisata extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct(){
		parent:: __construct();
		$this->load->model('modelwisata');
	}

	public function index()
	{
		$data['provinsi'] = $this->modelwisata->kabupaten();
		$this->load->view('wisata',$data);
	}

	function ambil_data(){


		$modul=$this->input->post('modul');
		$id=$this->input->post('id');

		if($modul=="kecamatan"){
		echo $this->modelwisata->kecamatan($id);

		}
		else if($modul=="kelurahan"){
		echo $this->modelwisata->kelurahan($id);
		}
	}

	public function simpan(){
		$kabupaten = $this->input->post('kabupaten');
		$kecamatan = $this->input->post('kecamatan');
		$kelurahan = $this->input->post('kelurahan');

		echo $kabupaten."<br>".$kecamatan."<br>".$kelurahan;
	}
}
