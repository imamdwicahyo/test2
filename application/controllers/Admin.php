<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class admin extends CI_Controller{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
   public function __construct(){
     parent::__construct();
     $this->load->model('modeladmin');
	 }

	public function index()
	{
    //mengambil data wisata sesuai username
    $username = $_SESSION['admin'];
    $data['wisata'] = $this->modeladmin->getDataWisata($username)->result();

    //cek apakah sudah Login
    //jika sudak masuk ke halaman Admin
    //jika belum kembali ke halaman Home
    $login = $this->autentikasi->cekLogin();
    if ($login) {
      $this->load->view('admin',$data);
    }else {
      redirect(base_url('home'));
    }

	}

  function export(){

          // Panggil class PHPExcel nya
          $excel = new PHPExcel();
          //$path = $_SERVER['DOCUMENT_ROOT'] . '/assets/gambar/';

          // Settingan awal fil excel
          $excel->getProperties()->setCreator('imam')
                                 ->setLastModifiedBy('imam')
                                 ->setTitle("Data Wisata")
                                 ->setSubject("Wisata")
                                 ->setDescription("Data Seluru Wisata")
                                 ->setKeywords("Data Wisata");

          // Buat sebuah variabel untuk menampung pengaturan style dari header tabel
           // Buat sebuah variabel untuk menampung pengaturan style dari header tabel
          $style_col = array(
              'fill' => array(
                      'type' => PHPExcel_Style_Fill::FILL_SOLID,
                      'color' => array('rgb'=>'E1E0F7'),
              ),
              'font' => array('bold' => true), // Set font nya jadi bold
              'alignment' => array(
                  'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
                  'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
              ),
              'borders' => array(
                  'outline' => array(
                      'style' => PHPExcel_Style_Border::BORDER_THIN,
                  ),
              ),
          );

          // Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
          $style_row = array(
              'alignment' => array(
                  'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
              ),
              'borders' => array(
                  'outline' => array(
                      'style' => PHPExcel_Style_Border::BORDER_THIN,
                  ),
              ),
          );

          $excel->setActiveSheetIndex(0)->setCellValue('A1', "Data Wisata"); // Set kolom A1 dengan tulisan "DATA SISWA"
          $excel->getActiveSheet()->mergeCells('A1:M1'); // Set Merge Cell pada kolom A1 sampai E1
          $excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
          $excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
          $excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1

          $excel->setActiveSheetIndex(0)->setCellValue('A3', "Tanggal Cetak : ".date("d F Y")); // Set kolom A3 dengan tulisan

          // Buat header tabel nya pada baris ke 3
          $excel->setActiveSheetIndex(0)->setCellValue('A4', "Kode"); // Set kolom A3 dengan tulisan
          $excel->setActiveSheetIndex(0)->setCellValue('B4', "Nama Wisata"); // Set kolom B3 dengan tulisan
          $excel->setActiveSheetIndex(0)->setCellValue('C4', "latitude"); // Set kolom C3 dengan tulisan
          $excel->setActiveSheetIndex(0)->setCellValue('D4', "Langitude"); // Set kolom D3 dengan tulisan
          $excel->setActiveSheetIndex(0)->setCellValue('E4', "Alamat"); // Set kolom D3 dengan tulisan
          $excel->setActiveSheetIndex(0)->setCellValue('F4', "No Telepon"); // Set kolom D3 dengan tulisan
          $excel->setActiveSheetIndex(0)->setCellValue('G4', "Kabupaten"); // Set kolom D3 dengan tulisan
          $excel->setActiveSheetIndex(0)->setCellValue('H4', "Kecamatan"); // Set kolom D3 dengan tulisan
          $excel->setActiveSheetIndex(0)->setCellValue('I4', "Kelurahan"); // Set kolom D3 dengan tulisan
          $excel->setActiveSheetIndex(0)->setCellValue('J4', "Deskripsi"); // Set kolom D3 dengan tulisan
          $excel->setActiveSheetIndex(0)->setCellValue('K4', "Tiket Dewasa"); // Set kolom D3 dengan tulisan
          $excel->setActiveSheetIndex(0)->setCellValue('L4', "Tiket Anak"); // Set kolom D3 dengan tulisan
          $excel->setActiveSheetIndex(0)->setCellValue('M4', "Pemilik"); // Set kolom D3 dengan tulisan

          // Apply style header yang telah kita buat tadi ke masing-masing kolom header
          $excel->getActiveSheet()->getStyle('A4')->applyFromArray($style_col);
          $excel->getActiveSheet()->getStyle('B4')->applyFromArray($style_col);
          $excel->getActiveSheet()->getStyle('C4')->applyFromArray($style_col);
          $excel->getActiveSheet()->getStyle('D4')->applyFromArray($style_col);
          $excel->getActiveSheet()->getStyle('E4')->applyFromArray($style_col);
          $excel->getActiveSheet()->getStyle('F4')->applyFromArray($style_col);
          $excel->getActiveSheet()->getStyle('G4')->applyFromArray($style_col);
          $excel->getActiveSheet()->getStyle('H4')->applyFromArray($style_col);
          $excel->getActiveSheet()->getStyle('I4')->applyFromArray($style_col);
          $excel->getActiveSheet()->getStyle('J4')->applyFromArray($style_col);
          $excel->getActiveSheet()->getStyle('K4')->applyFromArray($style_col);
          $excel->getActiveSheet()->getStyle('L4')->applyFromArray($style_col);
          $excel->getActiveSheet()->getStyle('M4')->applyFromArray($style_col);

          // Panggil function view yang ada di SiswaModel untuk menampilkan semua data siswanya
          $dataBarang = $this->modeladmin->getAllWisata()->result();
          $no = 1; // Untuk penomoran tabel, di awal set dengan 1
          $numrow = 5; // Set baris pertama untuk isi tabel adalah baris ke 5
          foreach($dataBarang as $data){ // Lakukan looping pada variabel siswa
              $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $data->kode);
              $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $data->nama);
              $excel->setActiveSheetIndex(0)->setCellValue('C'.$numrow, $data->latitude);
              $excel->setActiveSheetIndex(0)->setCellValue('D'.$numrow, $data->langitude);
              $excel->setActiveSheetIndex(0)->setCellValue('E'.$numrow, $data->alamat);
              $excel->setActiveSheetIndex(0)->setCellValue('F'.$numrow, $data->no_tlp);
              $excel->setActiveSheetIndex(0)->setCellValue('G'.$numrow, $data->kabupaten);
              $excel->setActiveSheetIndex(0)->setCellValue('H'.$numrow, $data->kecamatan);
              $excel->setActiveSheetIndex(0)->setCellValue('I'.$numrow, $data->kelurahan);
              $excel->setActiveSheetIndex(0)->setCellValue('J'.$numrow, $data->deskripsi);
              $excel->setActiveSheetIndex(0)->setCellValue('K'.$numrow, $data->harga_dewasa);
              $excel->setActiveSheetIndex(0)->setCellValue('L'.$numrow, $data->harga_anak);
              $excel->setActiveSheetIndex(0)->setCellValue('M'.$numrow, $data->username);

              // Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
              $excel->getActiveSheet()->getStyle('A'.$numrow)->applyFromArray($style_row);
              $excel->getActiveSheet()->getStyle('B'.$numrow)->applyFromArray($style_row);
              $excel->getActiveSheet()->getStyle('C'.$numrow)->applyFromArray($style_row);
              $excel->getActiveSheet()->getStyle('D'.$numrow)->applyFromArray($style_row);
              $excel->getActiveSheet()->getStyle('E'.$numrow)->applyFromArray($style_row);
              $excel->getActiveSheet()->getStyle('F'.$numrow)->applyFromArray($style_row);
              $excel->getActiveSheet()->getStyle('G'.$numrow)->applyFromArray($style_row);
              $excel->getActiveSheet()->getStyle('H'.$numrow)->applyFromArray($style_row);
              $excel->getActiveSheet()->getStyle('I'.$numrow)->applyFromArray($style_row);
              $excel->getActiveSheet()->getStyle('J'.$numrow)->applyFromArray($style_row);
              $excel->getActiveSheet()->getStyle('K'.$numrow)->applyFromArray($style_row);
              $excel->getActiveSheet()->getStyle('L'.$numrow)->applyFromArray($style_row);
              $excel->getActiveSheet()->getStyle('M'.$numrow)->applyFromArray($style_row);

              $no++; // Tambah 1 setiap kali looping
              $numrow++; // Tambah 1 setiap kali looping
          }

          // Set width kolom
          $excel->getActiveSheet()->getColumnDimension('A')->setWidth(5); // Set width kolom A
          $excel->getActiveSheet()->getColumnDimension('B')->setWidth(15); // Set width kolom B
          $excel->getActiveSheet()->getColumnDimension('C')->setWidth(25); // Set width kolom C
          $excel->getActiveSheet()->getColumnDimension('D')->setWidth(20); // Set width kolom D
          $excel->getActiveSheet()->getColumnDimension('E')->setWidth(5); // Set width kolom E
          $excel->getActiveSheet()->getColumnDimension('F')->setWidth(15); // Set width kolom B
          $excel->getActiveSheet()->getColumnDimension('G')->setWidth(15); // Set width kolom B
          $excel->getActiveSheet()->getColumnDimension('H')->setWidth(15); // Set width kolom B
          $excel->getActiveSheet()->getColumnDimension('I')->setWidth(15); // Set width kolom B
          $excel->getActiveSheet()->getColumnDimension('J')->setWidth(15); // Set width kolom B
          $excel->getActiveSheet()->getColumnDimension('K')->setWidth(15); // Set width kolom B
          $excel->getActiveSheet()->getColumnDimension('L')->setWidth(15); // Set width kolom B
          $excel->getActiveSheet()->getColumnDimension('M')->setWidth(15); // Set width kolom B

          // Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
          $excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);

          // Set orientasi kertas jadi LANDSCAPE
          $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

          // Set judul file excel nya
          $excel->getActiveSheet(0)->setTitle("Data Wisata");
          $excel->setActiveSheetIndex(0);

          // Proses file excel
          header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
          header('Content-Disposition: attachment; filename="Data Barang.xlsx"'); // Set nama file excel nya
          header('Cache-Control: max-age=0');

          $write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
          $write->save('php://output');

  }

  public function exportPDF(){
      $data['wisata'] = $this->modeladmin->getAllWisata()->result();
      $tgl = date("Y/m/d");
      $this->pdf->load_view('exportpdf',$data);
      set_time_limit (500);
      $this->pdf->set_paper('letter','landscape');
      $this->pdf->render();
      $this->pdf->stream("Data_Wisata".$tgl.".pdf");
  }

  public function imam(){
    echo "imam";
  }

}
