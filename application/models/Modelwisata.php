<?php

/**
 *
 */
class modelwisata extends CI_Model{

    public function getKabupaten(){
      $sql = "SELECT * FROM kabupaten ORDER BY nama ASC";
      $data = $this->db->query($sql);
      return $data;
    }

    public function getKecamatan($kabId){
      $sql = "SELECT * FROM kecamatan";
      $data = $this->db->query($sql);
      return $data;
    }

    public function getKelurahan($id){

    }

    function kabupaten(){


    $this->db->order_by('nama','ASC');
    $provinces= $this->db->get('kabupaten');


    return $provinces->result_array();


    }


    function kecamatan($provId){

    $kabupaten="<option value='0'>--pilih--</pilih>";

    $this->db->order_by('nama','ASC');
    $kab= $this->db->get_where('kecamatan',array('kode_kabupaten'=>$provId));

    foreach ($kab->result_array() as $data ){
    $kabupaten.= "<option value='$data[kode_kecamatan]'>$data[nama]</option>";
    }

    return $kabupaten;

    }


    function kelurahan($kecId){
    $kelurahan="<option value='0'>--pilih--</pilih>";

    $this->db->order_by('nama','ASC');
    $kel= $this->db->get_where('kelurahan',array('kode_kecamatan'=>$kecId));

    foreach ($kel->result_array() as $data ){
    $kelurahan.= "<option value='$data[kode_kelurahan]'>$data[nama]</option>";
    }

    return $kelurahan;
    }


}
