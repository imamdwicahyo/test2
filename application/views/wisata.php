<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title> Home </title>

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>">
    <script type="text/javascript" src="<?php echo base_url('assets/bootstrap/jquery.min.js') ?>"></script>
    <script type="text/javascript">

    $(function(){

    $.ajaxSetup({
    type:"POST",
    url: "<?php echo base_url('wisata/ambil_data') ?>",
    cache: false,
    });

    $("#kabupaten-kota").change(function(){
    var value=$(this).val();
    if(value!=0){
    $.ajax({
    data:{modul:'kecamatan',id:value},
    success: function(respond){
    $("#kecamatan").html(respond);
    }
    })
    }
    })

    $("#kecamatan").change(function(){
    var value=$(this).val();
    if(value!=0){
    $.ajax({
    data:{modul:'kelurahan',id:value},
    success: function(respond){
    $("#kelurahan-desa").html(respond);
    }
    })
    }
    })

    })

    </script>

  </head>
  <body>

  <!-- header -->
  <center>
  <h1> Header </h1>
    <table>
      <tr>
        <td> <a href="<?php echo base_url(); ?> "> <input type="button" name="Home" value="Home"> </a> </td>
        <td> <a href="<?php echo base_url('peta'); ?> "> <input type="button" name="Home" value="Peta"> </a> </td>
        <td> <a href="<?php echo base_url('wisata'); ?> "> <input type="button" name="Home" value="Wista"> </a> </td>
        <td> <a href="<?php echo base_url('home/doLogout'); ?> "> <input type="button" name="Home" value="Loguot"> </a> </td>
      </tr>
    </table>
  </center>

  <hr>

  <!-- Filter -->
  <form action="<?php echo base_url('wisata/simpan') ?>" method="post">
  <table>
    <tr>
      <td> Kabupaten : </td>
      <td>
        <select class='form-control' id='kabupaten-kota' name="kabupaten">
        <option value='0'>--pilih--</option>
        <?php
        foreach ($provinsi as $prov) {
        echo "<option value='$prov[kode_kabupaten]'>$prov[nama]</option>";
        }
        ?>
        </select>
      </td>
    </tr>
    <tr>
      <td> Kecamatan </td>
      <td>
        <select class='form-control' id='kecamatan' name="kecamatan">
        <option value='0'>--pilih--</option>
        </select>
      </td>
    </tr>
    <tr>
      <td> Kelurahan </td>
      <td>
        <select class='form-control' id='kelurahan-desa' name="kelurahan">
        <option value='0'>--pilih--</option>
        </select>
      </td>
    </tr>
  </table>
  <input type="submit" value="Simpan">
</form>

  </body>
</html>
