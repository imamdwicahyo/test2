<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title> Home </title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>">
    <script type="text/javascript" src="<?php echo base_url('assets/bootstrap/jquery.min.js') ?>"></script>
  </head>
  <body>

  <!-- Header -->
  <center>
  <h1> Header </h1>
    <table>
      <tr>
        <td> <a href="<?php echo base_url(); ?> "> <input type="button" name="Home" value="Home"> </a> </td>
        <td> <a href="<?php echo base_url('peta'); ?> "> <input type="button" name="Home" value="Peta"> </a> </td>
        <td> <a href="<?php echo base_url('wisata'); ?> "> <input type="button" name="Home" value="Wista"> </a> </td>
        <td> <a href="<?php echo base_url('home/doLogout'); ?> "> <input type="button" name="Home" value="Loguot"> </a> </td>
      </tr>
    </table>
  </center>

  <hr>

  <table border="1">
    <tr>
      <th> no </th>
      <th> nama </th>
      <th> latitude </th>
      <th> langitude </th>
      <th> alamat </th>
      <th> nomor telepon </th>
      <th> kabupaten </th>
      <th> kecamatan </th>
      <th> kelurahan </th>
      <th> deskripsi </th>
      <th> tiket dewasa </th>
      <th> tiket anak </th>
    </tr>
    <?php
      foreach ($wisata as $a) { ?>
        <tr>
          <td> <?php echo $a->kode; ?> </td>
          <td> <?php echo $a->nama; ?> </td>
          <td> <?php echo $a->latitude; ?> </td>
          <td> <?php echo $a->langitude; ?> </td>
          <td> <?php echo $a->alamat; ?> </td>
          <td> <?php echo $a->no_tlp; ?> </td>
          <td> <?php echo $a->kabupaten; ?> </td>
          <td> <?php echo $a->kecamatan; ?> </td>
          <td> <?php echo $a->kelurahan; ?> </td>
          <td> <?php echo $a->deskripsi; ?> </td>
          <td> <?php echo $a->harga_dewasa; ?> </td>
          <td> <?php echo $a->harga_anak; ?> </td>
        </tr>
    <?php  }
    ?>
  </table>

  <a href="<?php echo base_url('admin/export') ?>"> <input type="button" class="form-control" value="Export To Excel"> </a>
  <br>
  <a href="<?php echo base_url('admin/exportPDF') ?>"> <input type="button" class="form-control" value="Export To PDF"> </a>

  </body>
</html>
