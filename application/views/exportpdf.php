<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title> export </title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>">
    <script type="text/javascript" src="<?php echo base_url('assets/bootstrap/jquery.min.js') ?>"></script>
  </head>
  <body>
    <table border="1">
      <tr>
        <th> no </th>
        <th> nama </th>
        <th> latitude </th>
        <th> langitude </th>
        <th> alamat </th>
        <th> nomor telepon </th>
        <th> kabupaten </th>
        <th> kecamatan </th>
        <th> kelurahan </th>
        <th> deskripsi </th>
        <th> tiket dewasa </th>
        <th> tiket anak </th>
      </tr>
      <?php
        foreach ($wisata as $a) { ?>
          <tr>
            <td> <?php echo $a->kode; ?> </td>
            <td> <?php echo $a->nama; ?> </td>
            <td> <?php echo $a->latitude; ?> </td>
            <td> <?php echo $a->langitude; ?> </td>
            <td> <?php echo $a->alamat; ?> </td>
            <td> <?php echo $a->no_tlp; ?> </td>
            <td> <?php echo $a->kabupaten; ?> </td>
            <td> <?php echo $a->kecamatan; ?> </td>
            <td> <?php echo $a->kelurahan; ?> </td>
            <td> <?php echo $a->deskripsi; ?> </td>
            <td> <?php echo $a->harga_dewasa; ?> </td>
            <td> <?php echo $a->harga_anak; ?> </td>
          </tr>
      <?php  }
      ?>
    </table>
    <p> imam dwi cahyo</p>
  </body>
</html>
